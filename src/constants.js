export const SHOW_MODEL = 'SHOW_MODEL'; 
export const HIDE_MODEL = 'HIDE_MODEL'; 
export const ADD_STORY = 'ADD_STORY';
export const SEARCH_STORIES = 'SEARCH_STORIES'; 
export const CLEAR_QUERY = 'CLEAR_QUERY'; 
