import React from 'react';
import {render, screen, fireEvent } from '@testing-library/react';
import App from './App';

test('test if the modalTriggerButton is visible', () => {
  render(<App />);
  const newButton = screen.getByText(/Press Here To Write New Post/i);
  expect(newButton).toBeVisible();
});

test('test modal is visible when clicked on modalTriggerButton', () => {
  render(<App />);
  fireEvent.click(screen.getByText(/Press Here To Write New Post/i));
  expect(screen.getByRole('modal')).toBeInTheDocument();
});

test('on search, correct stories are visible', () => {
  render(<App />);
  const searchBar = screen.getByPlaceholderText('SEARCH POST');

  fireEvent.change(searchBar, {
    target: {value: 'The standard chunk of Lorem Ipsum'},
  });

  const stories = screen.getAllByRole('story');

  for (let i = 0; i < stories.length; i++) {
    expect(stories[i]).toHaveTextContent(/The standard chunk of Lorem Ipsum/i);
  }
});