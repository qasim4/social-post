import React from 'react';
import '@testing-library/jest-dom';
import {render, screen, fireEvent} from '@testing-library/react';
import NewPostform from './NewPostForm';
import { addStory } from '../../actions';

test('test if the data is submitted properly when dispatch action addStory', () => {
  const dispatch = jest.fn();
  render(<NewPostform dispatch={dispatch} />);

  const titleInput = screen.getByPlaceholderText('Title');
  const descriptionInput = screen.getByPlaceholderText('Description');

  const titleText = 'Test Title Text';
  const descriptionText = 'Test Description Text';

  fireEvent.change(titleInput, {
    target: {value: titleText},
  });

  fireEvent.change(descriptionInput, {
    target: {value: descriptionText},
  });

  fireEvent.click(screen.getByRole('addStory'));

  expect(dispatch).toHaveBeenCalled();
  expect(dispatch).toHaveBeenCalledWith(addStory(titleText, descriptionText, new Date().toLocaleString('en-GB')));
});

test('test if the data is blank in NewPostForm, submit button is disabled', () => {
  const dispatch = jest.fn();
  render(<NewPostform dispatch={dispatch} />);
  expect(screen.getByText(/submit/i)).toBeDisabled();
});

test('test when data is not blank in NewPostForm, submit button must be enable', () => {
  const dispatch = jest.fn();
  render(<NewPostform dispatch={dispatch} />);

  const titleInput = screen.getByPlaceholderText('Title');
  const descriptionInput = screen.getByPlaceholderText('Description');

  const titleText = 'Test Title Text';
  const descriptionText = 'Test Description Text';

  fireEvent.change(titleInput, {
    target: {value: titleText},
  });

  fireEvent.change(descriptionInput, {
    target: {value: descriptionText},
  });

  expect(screen.getByText(/submit/i).getAttribute("disabled")).toBe(null);
});


