/* eslint-disable jsx-a11y/aria-role */
import React, { useState } from "react";
import PropTypes from "prop-types";
import "./style.css";
import { addStory, hideModel } from "../../actions";

const Modal = ({ dispatch }) => {
  const [formData, setFormData] = useState({
    title: "",
    description: "",
  });

  const onChangeHandler = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  return (
    <div className="modal" role="modal">
      <section className="modal-main">
        <form className="form" autoComplete="off">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            name="title"
            placeholder="Title"
            value={formData.title}
            onChange={onChangeHandler}
          />
          <label htmlFor="description">Description</label>
          <textarea
            name="description"
            placeholder="Description"
            value={formData.description}
            onChange={onChangeHandler}
          />
          <input
            role="addStory"
            type="button"
            value="Submit"
            onClick={() => {
              dispatch(
                addStory(
                  formData.title,
                  formData.description,
                  new Date().toLocaleString("en-GB")
                )
              );
              dispatch(hideModel());
            }}
            disabled={
              formData.title.trim() === "" || formData.description.trim() === ""
            }
          />
          <input
            type="button"
            value="Cancel"
            className="form-cancel-button"
            onClick={() => dispatch(hideModel())}
          />
        </form>
      </section>
    </div>
  );
};

Modal.propTypes = {
  dispatch: PropTypes.func,
};

export default Modal;
