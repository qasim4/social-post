import React, { useState } from "react";
import PropTypes from "prop-types";
import "./style.css";
import searchIcon from "../../images/search.svg";
import crossIcon from "../../images/cross.svg";
import { searchStories, clearQuery } from "../../actions";

const escapeRegularExp = string => string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&');

const SearchBar = ({ dispatch }) => {
  const [value, setValue] = useState("");
  return (
    <div className="search-container">
      <img className="search-icon" src={searchIcon} alt="icon" />
      <input
        className="search-input-field"
        type="text"
        placeholder="SEARCH POST"
        value={value}
        onChange={(e) => {
          dispatch(searchStories(escapeRegularExp(e.target.value)));
          setValue(e.target.value);
        }}
      />
      {value !== "" ? (
        <img
          className="search-icon search-icon-right"
          src={crossIcon}
          alt="crossIcon"
          onClick={() => {
            setValue("");
            dispatch(clearQuery());
          }}
        />
      ) : null}
    </div>
  );
};

SearchBar.propTypes = {
  dispatch: PropTypes.func,
};

export default SearchBar;
