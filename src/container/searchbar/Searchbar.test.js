import React from 'react';
import '@testing-library/jest-dom';
import {render, screen, fireEvent} from '@testing-library/react';
import App from '../../App';
import  Searchbar from './Searchbar';
import { clearQuery, searchStories } from '../../actions';

test('on clear the search box value is empty', () => {
  render(<App />);

  const searchBox = screen.getByPlaceholderText('SEARCH POST');

  fireEvent.change(searchBox, {
    target: {value: 'Test'},
  });

  fireEvent.click(screen.getByAltText('crossIcon'));

  expect(screen.getByPlaceholderText('SEARCH POST')).toHaveValue('');
});

test('searchStories action is triggered on change the text of searchbar with correct text', () => {
  const dispatch = jest.fn();
  render(<Searchbar dispatch={dispatch}/>);

  const searchBox = screen.getByPlaceholderText('SEARCH POST');
  const searchText = 'test';
  
  fireEvent.change(searchBox, {
    target: { value : searchText},
  });

  expect(dispatch).toHaveBeenCalled();
  expect(dispatch).toHaveBeenCalledWith(searchStories(searchText));
});

test('clear query action is triggered on cancel click', () => {
  const dispatch = jest.fn();
  render(<Searchbar dispatch={dispatch}/>);
  const searchBox = screen.getByPlaceholderText('SEARCH POST');

  fireEvent.change(searchBox, {
    target: {value: 'Test'},
  });
  
  fireEvent.click(screen.getByAltText('crossIcon'));
  expect(dispatch).toHaveBeenCalled();
  expect(dispatch).toHaveBeenCalledWith(clearQuery());
});