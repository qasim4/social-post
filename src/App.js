import React, { useReducer } from "react";
import "./App.css";
import SearchBar from "./container/searchbar/Searchbar";
import ModalTriggerButton from "./component/modalTriggerButton/ModalTriggerButton";
import PostCard from "./component/post-card/PostCard";
import reducer from "./reducer";
import NewPostForm from "./container/new-post-form/NewPostForm";

const initialState = {
  stories: [
    {
      title: "The standard chunk of Lorem Ipsum",
      description:
        "Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.",
      timeStamp: new Date().toLocaleString("en-GB"),
    },
  ],
  isModelOpen: false,
  query: "",
};

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="App">
      <div className="container">
        <header className="header">
          <SearchBar dispatch={dispatch} />
        </header>
        <section className="mid-section">
          <ModalTriggerButton value={state.isModelOpen} dispatch={dispatch} />
          {state.isModelOpen && <NewPostForm dispatch={dispatch} />}
        </section>
        <section className="post-section">
          {state.query !== ""
            ? state.stories
                .filter((item) =>
                  item.title.match(new RegExp(state.query, "i")) ||
                  item.description.match(new RegExp(state.query, "i"))
                    ? true
                    : false
                )
                .map((item, index) => (
                  <PostCard
                    title={item.title}
                    description={item.description}
                    timeStamp={item.timeStamp}
                    key={index}
                  />
                ))
            : state.stories.map((item, index) => (
                <PostCard
                  title={item.title}
                  description={item.description}
                  timeStamp={item.timeStamp}
                  key={index}
                />
              ))}
        </section>
      </div>
    </div>
  );
}

export default App;
