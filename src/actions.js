import {
  SHOW_MODEL,
  HIDE_MODEL,
  ADD_STORY,
  SEARCH_STORIES,
  CLEAR_QUERY,
} from './constants';

export const showModel = () => ({
  type: SHOW_MODEL,
});

export const hideModel = () => ({
  type: HIDE_MODEL,
});

export const addStory = (title, description, timeStamp) => ({
  type: ADD_STORY,
  payload: {
    title,
    description,
    timeStamp,
  },
});

export const searchStories = query => ({
  type: SEARCH_STORIES,
  payload: {
    query, 
  },
});

export const clearQuery = () => ({
  type: CLEAR_QUERY,
});