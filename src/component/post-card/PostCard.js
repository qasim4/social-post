/* eslint-disable jsx-a11y/aria-role */
import React from "react";
import "./style.css";
import PropTypes from "prop-types";
import ReactMarkdown from "react-markdown";

const Card = ({ title, description, timeStamp }) => (
  <div className="card" role="story">
    <div className="card-top">
      <div className="card-display-block">
        <h5 className="card-title">{title}</h5>
      </div>
      <div className="card-display-block card-time">{timeStamp}</div>
    </div>
    <div className="card-description">
      <ReactMarkdown source={description} />
    </div>
  </div>
);

Card.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  timeStamp: PropTypes.string,
};

export default Card;
