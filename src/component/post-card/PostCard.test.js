import React from 'react';
import '@testing-library/jest-dom';
import {render, screen } from '@testing-library/react';
import PostCard from './PostCard';

test('all the data passed in props to PostCard component are visible', () => {
  render(<PostCard title="title" description="description" timeStamp='06/07/2020, 1:43:39' />);

  expect(screen.getByText('title')).toBeVisible();
  expect(screen.getByText('description')).toBeVisible();
  expect(screen.getByText('06/07/2020, 1:43:39')).toBeVisible();
});


