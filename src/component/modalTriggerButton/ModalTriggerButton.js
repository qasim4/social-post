import React from "react";
import "./style.css";
import PropTypes from "prop-types";
import { showModel } from "../../actions";

const ModalTriggerButton = ({ dispatch }) => (
  <button
    className="modal-trigger-button"
    onClick={() => dispatch(showModel())}
  >
    Press Here To Write New Post
  </button>
);

ModalTriggerButton.propTypes = {
  dispatch: PropTypes.func,
};

export default ModalTriggerButton;
