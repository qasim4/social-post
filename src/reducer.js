import {
  SHOW_MODEL,
  HIDE_MODEL,
  ADD_STORY,
  SEARCH_STORIES,
  CLEAR_QUERY,
} from './constants';

const reducer = (state, action) => {
  switch (action.type) {
    case SHOW_MODEL:
      return {
        ...state,
        isModelOpen: true,
      };
    case HIDE_MODEL:
      return {
        ...state,
        isModelOpen: false,
      };
    case ADD_STORY:
      const stories = state.stories;
      stories.unshift(action.payload);
      return {
        ...state,
        stories: stories,
      };
    case SEARCH_STORIES:
      return {
        ...state,
        query: action.payload.query,
      };
    case CLEAR_QUERY: 
      return {
        ...state,
        query: '',
      };
    default:
      throw new Error();
  }
};

export default reducer;