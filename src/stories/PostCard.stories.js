import React from "react";
import PostCard from "../component/post-card/PostCard";

export default {
  title: "Post-Card",
  component: PostCard,
};

export const card = () => (
  <PostCard
    title="Title"
    description="- First item
- Second item
- Third item
    - Indented item
    - Indented item
- Fourth item"
    timeStamp="06/07/2020, 1:43:39"
  />
);
