import React from "react";
import NewPostForm from "../container/new-post-form/NewPostForm";
import { action } from "@storybook/addon-actions";

export default {
  title: "NewPostForm",
  component: NewPostForm,
};

export const newPostForm = () => <NewPostForm dispatch={action("onClick")} />;
