import React from 'react';
import Searchbar from '../container/searchbar/Searchbar';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Searchbar',
  component: Searchbar,
};

export const searchbar = () => <Searchbar dispatch={action('onChange')}/>;
