import React from 'react';
import ModalTriggerButton from '../component/modalTriggerButton/ModalTriggerButton';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Trigger Button',
  component: ModalTriggerButton,
};

export const modalTriggerButton = () => <ModalTriggerButton dispatch={action('clicked')}/>;
